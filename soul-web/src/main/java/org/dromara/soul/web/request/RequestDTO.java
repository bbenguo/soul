/*
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements.  See the NOTICE file distributed with
 *   this work for additional information regarding copyright ownership.
 *   The ASF licenses this file to You under the Apache License, Version 2.0
 *   (the "License"); you may not use this file except in compliance with
 *   the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

package org.dromara.soul.web.request;

import lombok.Data;
import org.dromara.soul.common.constant.Constants;
import org.dromara.soul.common.enums.HttpMethodEnum;
import org.dromara.soul.common.enums.RpcTypeEnum;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * the soul request DTO .
 *
 * @author xiaoyu(Myth)
 */
@Data
public class RequestDTO implements Serializable {

    private static List<String> defaultFields = Arrays.asList(
            Constants.APP_KEY, Constants.DUBBO_RPC_RESULT, Constants.DECODE,
            Constants.MODULE, Constants.METHOD, Constants.EXT_INFO,
            Constants.RPC_TYPE, Constants.SIGN, Constants.TIMESTAMP);

    private String context;

    private String version = "v1";

    /**
     * is module data.
     */
    private String module;

    /**
     * is method name .
     */
    private String method;

    /**
     * is rpcType data. now we only support "http","dubbo" "springCloud".
     * {@linkplain RpcTypeEnum}
     */
    private String rpcType;

    /**
     * httpMethod now we only support "get","post" .
     * {@linkplain  HttpMethodEnum}
     */
    private String httpMethod;

    /**
     * this is dubbo params.
     */
    private String dubboParams;

    /**
     * this is sign .
     */
    private String sign;

    /**
     * timestamp .
     */
    private String timestamp;

    /**
     * appKey .
     */
    private String appKey;

    /**
     * content is json data.
     */
    private String content;

    /**
     * extInfo is json data .
     */
    private String extInfo;

    /**
     * pathVariable
     */
    private String pathVariable;

    /**
     * ServerHttpRequest transform RequestDTO .
     *
     * @param request {@linkplain ServerHttpRequest}
     * @return RequestDTO request dto
     */
//    public static RequestDTO transform(final ServerHttpRequest request) {
//        final String module = request.getHeaders().getFirst(Constants.MODULE);
//        final String method = request.getHeaders().getFirst(Constants.METHOD);
//        final String appKey = request.getHeaders().getFirst(Constants.APP_KEY);
//        final String httpMethod = request.getHeaders().getFirst(Constants.HTTP_METHOD);
//        final String rpcType = request.getHeaders().getFirst(Constants.RPC_TYPE);
//        final String sign = request.getHeaders().getFirst(Constants.SIGN);
//        final String timestamp = request.getHeaders().getFirst(Constants.TIMESTAMP);
//        final String extInfo = request.getHeaders().getFirst(Constants.EXT_INFO);
//        final String pathVariable = request.getHeaders().getFirst(Constants.PATH_VARIABLE);
//        RequestDTO requestDTO = new RequestDTO();
//        requestDTO.setModule(module);
//        requestDTO.setMethod(method);
//        requestDTO.setAppKey(appKey);
//        requestDTO.setHttpMethod(httpMethod);
//        requestDTO.setRpcType(rpcType);
//        requestDTO.setSign(sign);
//        requestDTO.setTimestamp(timestamp);
//        requestDTO.setExtInfo(extInfo);
//        requestDTO.setPathVariable(pathVariable);
//        return requestDTO;
//    }
    public static RequestDTO transform(final ServerHttpRequest request) {
        RequestPath requestPath = request.getPath();
        String context = requestPath.subPath(1, 2).value();
        String version = requestPath.subPath(3, 4).value();
        String module = requestPath.subPath(5, 6).value(); //request.getQueryParams().getFirst(Constants.MODULE);
        String method = requestPath.subPath(4).value(); //request.getQueryParams().getFirst(Constants.METHOD);

        String appKey = request.getQueryParams().getFirst(Constants.APP_KEY);
        String httpMethod = request.getMethodValue().toLowerCase(); // request.getQueryParams().getFirst(Constants.HTTP_METHOD);
        String rpcType = request.getQueryParams().getFirst(Constants.RPC_TYPE);
        if (StringUtils.isEmpty(rpcType)) {
            rpcType = RpcTypeEnum.SPRING_CLOUD.getName();
        }
        String sign = request.getQueryParams().getFirst(Constants.SIGN);
        String timestamp = request.getQueryParams().getFirst(Constants.TIMESTAMP);
        String extInfo = makeExtraInfo(request.getQueryParams());
        String pathVariable = request.getQueryParams().getFirst(Constants.PATH_VARIABLE);
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setContext(context);
        requestDTO.setVersion(version);
        requestDTO.setModule(module);
        requestDTO.setMethod(method);
        requestDTO.setAppKey(appKey);
        requestDTO.setHttpMethod(httpMethod);
        requestDTO.setRpcType(rpcType);
        requestDTO.setSign(sign);
        requestDTO.setTimestamp(timestamp);
        requestDTO.setExtInfo(extInfo);
        requestDTO.setPathVariable(pathVariable);
        return requestDTO;
    }

    public static RequestDTO transformMap(MultiValueMap<String, String> queryParams) {
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setModule(queryParams.getFirst(Constants.MODULE));
        requestDTO.setMethod(queryParams.getFirst(Constants.METHOD));
        requestDTO.setRpcType(queryParams.getFirst(Constants.RPC_TYPE));
        return requestDTO;
    }

    private static String makeExtraInfo(MultiValueMap<String, String> queryParams) {
        return org.apache.commons.lang3.StringUtils.join(
                queryParams.entrySet().stream()
                        .filter(entry -> !defaultFields.contains(entry.getKey()))
                        .map(entry -> entry.getKey() + "=" + queryParams.getFirst(entry.getKey())).collect(Collectors.toList()), "&"
        );
    }

}
